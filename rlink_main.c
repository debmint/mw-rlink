/* rlink_main.c - mainline routine to build rlink */

#define MAIN

#include <stdlib.h>
#include "rlink.h"

int etrap();

#ifdef __STDC__
static void B09fatal (char *msg, char *fmt);
static char *basenam (register char *arg);
#else
char *basenam();
static void B09fatal ();
#endif

int
#ifdef __STDC__
main (int argc, char **argv)
#else
main (argc, argv)
    int argc; char **argv;
#endif
{
    int curopt;
    int parmlen;
    int v2;
    int v1;
    unsigned int v0;

#ifdef COCO
    intercept (etrap);
#endif
    filenames = argv;

    while (--argc > 0)
    {
        register char *curarg;

        if (*(curarg = *(++argv)) == '-')
        {
            ++curarg;
            parmlen = 0;

            while (curopt = *(curarg++))
            {
                if (parmlen != 0)
                {
#ifdef COCO
                    fatal("unused '=' in %s", *argv);
#else
                    fatal("unused '=' in %s", *argv, 0, 0);
#endif
                }

                if (*curarg == '=')
                {
                    ++curarg;
                    parmlen = 1;
                }

                switch (curopt)
                {
                    case 'b':       /* Basic09 entry point */
                        B09Mod = 1;

                        if (parmlen)
                        {
                            B09EntPt = curarg;
                        }
                        break;
                    case 'E':
                    case 'e':       /* Edition           */
                        edition = atoi (curarg);
                        break;
                    case 'l':       /* library           */
                        if (lfilenum < 16)
                        {
                            lfiles[lfilenum++] = curarg;
                        }
                        else
                        {
                            fatal ("too many libraries: -l%s\n", curarg
#ifndef COCO
                            , 0, 0
#endif
                            );
                        }
                        break;
                    case 'M':       /* Extra memory      */
                        v0 = req_mem (curarg);

                        if (v0 >= 256)
                        {
                            extramem = v0;
                        }
                        break;
                    case 'm':       /* Print linkage map */
                        printmap = 1;
                        continue;
                    case 'n':       /* Name */
                        modname = curarg;
                        break;
                    case 'o':       /* Output object file */
                        OutfName = curarg;

                        if (modname == 0)
                        {
                            modname = basenam (curarg);
                        }
                        break;
                    case 's':       /* Print symbol table */
                        printsym = 1;
                        printmap = 1;
                        continue;  /* To inner while */
                    case 't':       /* Allow static data in BASIC09 modules */
                        okstatic = 1;
                        continue;  /* To inner while */
                    case 'z':
                        if (parmlen)
                        {
                            NameFile = curarg;
                            break;   /* To inner while */
                        }
                        continue;
                    default:
                        fatal ("unknown option %c in %s",
                                (char *)curopt, *argv
#ifndef COCO
                                , 0
#endif
                                );
                        break;
                }
                break;
            }
        }
        else        /* no '-', it's a filename */
        {
            filenames[FileCount++] = curarg;
        }
    }
    /* This conditional could be removed when need to compare
     * with original is no longer expected */
#ifndef COCO
    /* This comes later in original but should be done now */
    if (!OutfName)
    {
        fatal("no output file", 0, 0, 0);
    }

    if ((FileCount == 0) && !NameFile)
        fatal("no input files", 0, 0, 0);

#endif

    readrofs();   /* 02f0 */

    if (unrsolved())
    {
#ifdef COCO
        fatal("unresolved references");
#else
        fatal("unresolved references", 0 ,0, 0);
#endif
    }

    namelen = strlen (modname);
    adj_ofst();
    adjsmofst();

    if (B09Mod) /* else L0345 */
    {
        if (t_idat ||  t_idpd)
        {
            B09fatal ("no init data allowed"
#ifndef COCO
                    ,0
#endif
                    );
        }

        if ( t_udpd)
        {
            B09fatal ("no dp data allowed"
#ifndef COCO
                    ,0
#endif
                    );
        }

        if (!okstatic && t_udat)
        {
            B09fatal ("no static data"
#ifndef COCO
                    ,0
#endif
                    );
        }
    }

    if (( t_udpd +  t_idpd > 256)) /* L0345 */
    {
        fatal ("direct page allocation is %u bytes",
                (char *)( t_udpd +  t_idpd)
#ifndef COCO
                ,0, 0
#endif
                );
    }

    if (dup_find)  /* L035c */
    {
        fatal ("name clash"
#ifndef COCO
                ,0, 0, 0
#endif
                );
    }

    if (!OutfName)
    {
        fatal("no output file"
#ifndef COCO
                ,0,0,0
#endif
                );
    }

    if (B09Mod) /* L0378 - else L03c3 */
    {
        if (! B09EntPt)
        {
            B09fatal ("no entry point name"
#ifndef COCO
                    ,0
#endif
                    );  /* jump to L03be */
        }
        else
        {
            SYMDAT *p0 =  matchsym (B09EntPt);     /* some struct pointer */
           
            if ((p0 != 0) && ((p0->Type & 7) == CODENT))
            {   /* Analyze struct */
                D002f = p0->Offset;
            }
            else
            {
                B09fatal ("entry point '%s' not found", B09EntPt);
            }
        }
        
        TyLang = 0x2181;    /* L03be */
    }
    
    wrtmodule ();   /* L03c3 */
}

/* ================================================================ *
 * Handles opening of file ptr and allocates memory for the file    *
 * structure                                                        *
 * First gets all files from commandline, then reads from names     *
 * file if provided by "z" option                                   *
 * Returns pointer to filename                                      *
 * ================================================================ */

char *
nxtfnam ()
{
    char *fnam;

    if (CurntFil < FileCount)   /* filenames come from command line */
    {
        return filenames[CurntFil++];
    }

    if (!NameFile) /* filenames come from a file */
    {
        return 0;
    }

    if (!NamFilFP)
    {
        if ((NamFilFP = fopen(NameFile, "r")) == 0)
        {
            fatal("can't open '%s' name file", NameFile
#ifndef COCO
                    , 0, 0
#endif
                    );
        }
    }

    if (_fgets(fnam = get_mem (32), NamFilFP))
    {
        return fnam;
    }

    return 0;
}

char *
nxtlib ()
{
    if (LibFile < lfilenum)   /* else L045c */
    {
        return lfiles[LibFile++];
    }

    return 0;
}

static void
#ifdef __STDC__
B09fatal (char *msg, char *fmt)
#else
B09fatal (msg, fmt)
char *msg, *fmt;
#endif
{
/*    int m1;
    int m2;*/

    fprintf (stderr, msg, fmt);
    putc ('\n', stderr);
    fatal ("BASIC09 conflict"
#ifndef COCO
            , 0, 0, 0
#endif
            );
}

void
illmainline ()
{
    B09fatal ("no mainline allowed"
#ifndef COCO
            ,0
#endif
            );
}

void
cantopen ()
{
    fatal ("can't open '%s'", RofFil
#ifndef COCO
            , 0, 0
#endif
            );
}

void
notROF ()
{
    fatal ("'%s' is not a relocatable module", RofFil
#ifndef COCO
            , 0, 0
#endif
            );
}

void
asmerrs ()
{
    fatal ("'%s' contains assembly errors", RofFil
#ifndef COCO
            , 0, 0
#endif
            );
}

void
toonew ()
{
    fatal ("'%s' is too new for this version of assembler", RofFil
#ifndef COCO
            , 0, 0
#endif
            );
}

void
twomains ()
{
    fatal ("mainline found in both %s and %s", FirstMod->roffile, RofFil
#ifndef COCO
            , 0
#endif
            );
}

void
in_rderr ()
{
    fatal ("error reading input file %s", RofFil
#ifndef COCO
            , 0, 0
#endif
            );
}

static char *
#ifdef __STDC__
basenam (register char *arg)
#else
basenam (arg)
    register char *arg;
#endif
{
    int   curchr;
    char *path = arg;

    while ((curchr = *(arg++)))
    {
        if (curchr == '/')
        {
            path = arg;
        }
    }

    return path;
}

#ifdef __LIL_END__
unsigned int
o9_int(UINT val)
{
    return ((val & 0xff00) >> 8) + ((val & 0xff) << 8);
}
#endif
