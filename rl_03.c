#include "/dd/defs/module.h"
#ifndef COCO
#include <unistd.h>
#include <sys/types.h>
#endif
#include "rlink.h"

char      idpdBuf[256];
char      _Crc[3] = { '\xff', '\xff', '\xff' };
char     *D0084 = idpdBuf;
LONGTYPE  cod_end;
LONGTYPE  IDatLoc;
LONGTYPE  IDpdLoc;
LONGTYPE  DTRefLoc;

#ifdef __STDC__
void creatdnodes(int);
#else
void creatdnodes();
#endif

#ifdef COCO
#   define WMODE "wx+"
#else
#   define WMODE "w+"
#endif

void
wrtmodule()
{
    mod_exec mod_hdr;
    char *mod_nm;

    inpBuf = get_mem(obmemreq = maxbufsiz);
    obmemreq = 0;
    mod_hdr.m_sync = MODSYNC;
    mod_hdr.m_size = o9_int(((namelen + t_drefs) * 2) + 13 + t_code + t_idat +  t_idpd + 13 );

    /*fprintf(stderr, "namelen: %d\n", namelen);
    fprintf(stderr, "t_drefs: %d\n", t_drefs);
    fprintf(stderr, " t_code: %d\n", t_code);
    fprintf(stderr, " t_idat: %d\n", t_idat);
    fprintf(stderr, " t_idpd: %d\n", t_idpd);*/
    mod_hdr.m_name = o9_int(0x0d);
    /* The following cast makes the code match the original,
     * but, in reality, it would probably be better as an unsigned
     * because we'd never want signed shift here */
    mod_hdr.m_tylan = ((int)TyLang) >> 8;
    mod_hdr.m_attrev = TyLang & 0xff;
    mod_hdr.m_parity = pty_gen((char *)(&mod_hdr.m_sync), 8);

    mod_hdr.m_exec = o9_int(B09Mod ? D002f : FirstMod->entry + bgn_codsiz);
    mod_hdr.m_store = o9_int(t_idpd + t_udpd + t_idat + t_udat + extramem);

    if (!(OutFile = fopen(OutfName, WMODE)))
    {
        fatal("can't create output file"
#ifndef COCO
                , 0, 0, 0
#endif
                );
    }

#ifdef COCO
    OutFile->_bufsiz = 512;
#endif

    if (ftruncate(fileno(OutFile), o9_int(mod_hdr.m_size)))
    {
        fatal("can't set file size"
#ifndef COCO
                , 0, 0, 0
#endif
                );
    }

    /* Write module header to OutFile */
#ifdef COCO
    crc((char *)(&mod_hdr.m_sync), 13, _Crc);
    fwrite(&mod_hdr.m_sync, sizeof(mod_exec), 1, OutFile);
#else
    /* The write has to be split up because of bit alignment */
    fwrite(&mod_hdr, 8, 1, OutFile);
    crc((char *)(&mod_hdr), 8, _Crc);
    fputc((int)mod_hdr.m_parity, OutFile);
    crc((char *)(&mod_hdr.m_parity), 1, _Crc);
    fwrite(&mod_hdr.m_exec, 4, 1, OutFile);
    crc((char *)(&mod_hdr.m_exec), 4, _Crc);
#endif
    writ_err();

    /* Write module name to header */
    mod_nm = modname;

    while(*(mod_nm++));
    
    mod_nm -= 2;    /* return to last char of name string */
    *mod_nm |= 0x80;    /* Last char in modname has hi bit set */
    crc(modname, namelen, _Crc);
    fprintf(OutFile, modname);
    writ_err();
    crc(&edition, 1, _Crc);
    putc(edition, OutFile);
    *mod_nm &= 0x7f;    /* Reset last char of modname */

    creatdnodes(t_drefs);   /* Create all necessary dnodes */
    cod_end = ftell(OutFile);
    IDpdLoc = cod_end + t_code;
    IDatLoc = IDpdLoc + 2 + t_idpd + 2;
    DTRefLoc = IDatLoc + t_idat;
    curObfil = FirstMod;

    while (curObfil)
    {
        if (curObfil->x19 & 0x100)
        {
            L14d3();
            writ_err();
        }

        curObfil = curObfil->obn;
    }

    /* Init DPD */
    fseek(OutFile, IDpdLoc, 0);
#ifdef __LIL_END__
    t_idpd = o9_int(t_idpd);
#endif
    crc((char *)&t_idpd, 2, _Crc);
#ifdef __LIL_END__
    t_idpd = o9_int(t_idpd);
#endif
    putwrd(t_idpd, OutFile);
    crc(idpdBuf, t_idpd, _Crc);
    fwrite(idpdBuf, 1, t_idpd, OutFile);  /* Write all idpd data */
    writ_err();
#ifdef __LIL_END__
    t_idat = o9_int(t_idat);
#endif
    crc((char *)&t_idat, 2, _Crc);
#ifdef __LIL_END__
    t_idat = o9_int(t_idat);
#endif
    putwrd(t_idat, OutFile);

    if (t_idat)
    {
        idat_crc(t_idat);
    }

    /* Data-Text Refs */
    fseek(OutFile, DTRefLoc, 0);
#ifdef __LIL_END__
    dtcount = o9_int(dtcount);
#endif
    crc((char *)&dtcount, 2, _Crc);
#ifdef __LIL_END__
    dtcount = o9_int(dtcount);
#endif
    putwrd(dtcount, OutFile);     /* Write Data-Text count */
    writrefs(dtxtrefs);
    /* Data-Data Refs */
#ifdef __LIL_END__
    ddcount = o9_int(ddcount);
#endif
    crc((char *)&ddcount, 2, _Crc);
#ifdef __LIL_END__
    ddcount = o9_int(ddcount);
#endif
    putwrd(ddcount, OutFile);     /* Write Data-Data count */
    writrefs(ddtarefs);

    crc(modname, namelen + 1, _Crc);
    fprintf(OutFile, modname);
    
    putc(0, OutFile);
    _Crc[0] = ~_Crc[0];
    _Crc[1] = ~_Crc[1];
    _Crc[2] = ~_Crc[2];
    fwrite(_Crc, 3, 1, OutFile);
    writ_err();
    fclose(OutFile);

    if (printmap)
        writemap();

    if (B09Mod && okstatic)
        fprintf(stderr, "BASIC09 static data size is %d bytes\n", t_udat);
}

/* Load idat and calculate CRC for it */
void
idat_crc(tcount)
    int tcount;
{
    char buf[128];
    int nbyts;

    fseek(OutFile, ftell(OutFile), 0);
    
    while ((tcount > 0) && (nbyts = fread(buf, 1, sizeof(buf), OutFile))) 
    {
        writ_err();

        if (nbyts > tcount)
            nbyts = tcount;

        crc(buf, nbyts, _Crc);
        tcount -= nbyts;
    }
}

void
L14d3()
{
    SYMDAT *thissym;
    def_ref syminf;
    int     totcnt;

    if (curRofNa != curObfil->roffile) /* else L1513 */
    {
        if (!(InFile = freopen((curRofNa = curObfil->roffile), "r", InFile)))
            fatal("can't reopen input file %s", curObfil->roffile
#ifndef COCO
                , 0, 0
#endif
                );
    }

    fseek(InFile, curObfil->rcodbgn, 0);
    
    /* Read in code + init data */
    if (curObfil->codesiz + curObfil->IDpD + curObfil->IDat > 0)
    {
        if (!(fread(inpBuf, curObfil->codesiz + curObfil->IDpD + curObfil->IDat,
                        1, InFile)))
        {
            read_err(curObfil->roffile);
        }
    }

    /* Do External Refs */
    totcnt = getwrd(InFile);

    if (ferror(InFile))   /* else L15f3 */
    {
        read_err(curObfil->roffile);
    }

    while (totcnt--)
    {
        char mynam[SYMLEN+1];
        register int symcnt;

        getname(mynam, SYMLEN);
        symcnt = getwrd(InFile);

        if (ferror(InFile))
        {
            read_err(curObfil->roffile);
        }

        if (!(thissym = matchsym(mynam)))   /* else L15e9 */
            fatal("symbol %s not found in codgen", mynam
#ifndef COCO
                , 0, 0
#endif
                );

        while (symcnt--)
        {
#ifdef COCO
            fread(&syminf, 3, 1, InFile);
#else
            syminf.r_flag = fgetc(InFile);
            syminf.r_offset = getwrd(InFile);
#endif

            if (ferror(InFile))
                read_err(curObfil->roffile);

            procref(&syminf, thissym);
        }
    }   /* @L15f3 */

    /* Local Refs */
    totcnt = getwrd(InFile);

    if (ferror(InFile))
        read_err(curObfil->roffile);

    while (totcnt--)
    {
#ifdef COCO
        fread (&syminf, 3, 1, InFile);
#else
        syminf.r_flag = fgetc(InFile);
        syminf.r_offset = getwrd(InFile);
#endif

        if (ferror(InFile))
            read_err(curObfil->roffile);

        procref(&syminf, 0);
    }

    /* Common Data */
    if (curObfil->spare > 0)   /* else L1708 */
    {
        totcnt = getwrd(InFile);

        while (totcnt--)
        {
            char nam[10];
            register int count;

            getname(nam, 9);
            count = getwrd(InFile);

            if (ferror(InFile))
                read_err(curObfil->roffile);

            if (!(thissym =  matchsym(nam)))
                fatal("common block %s not found", nam
#ifndef COCO
                , 0, 0
#endif
                );

            count = getwrd(InFile);

            while (count--)
            {
                fread(&syminf, 3, 1, InFile);

                if (ferror(InFile))
                    read_err(curObfil->roffile);

                procref(&syminf, thissym);
            }
        }
    }

    crc(inpBuf, curObfil->codesiz, _Crc);

    if (!D0061)
        fseek(OutFile, cod_end, 0);

    /* Write code to module */
    fwrite(inpBuf, curObfil->codesiz, 1, OutFile);  /* L1737 */
    cod_end += curObfil->codesiz;
    D0061 = 1;
    writ_err();
    totcnt = 0;
    {
        register char *cp = &inpBuf[curObfil->codesiz];

        while (totcnt < curObfil->IDpD)
        {
            *(D0084++) = *(cp++);
            ++totcnt;
        }
    }

    if (curObfil->IDat)        /* else L1800 */
    {
        fseek(OutFile, IDatLoc, 0);
        fwrite(&inpBuf[curObfil->codesiz + curObfil->IDpD], curObfil->IDat,
                1, OutFile);
        IDatLoc += curObfil->IDat;
        writ_err();
        D0061 = 0;
    }

    bgn_udat += curObfil->UDat;
    bgn_udpd += curObfil->UDpD;
    bgn_idat += curObfil->IDat;
    bgn_idpd += curObfil->IDpD;
    bgn_codsiz += curObfil->codesiz;
    curObfil->UDat = bgn_udat - curObfil->UDat;
    curObfil->UDpD = bgn_udpd - curObfil->UDpD;
    curObfil->IDat = bgn_idat - curObfil->IDat;
    curObfil->IDpD = bgn_idpd - curObfil->IDpD;
    curObfil->codesiz = bgn_codsiz - curObfil->codesiz;
}

/* Process a ref */

void
procref(b3, ob)
    def_ref *b3; SYMDAT *ob;
{
    union {
        int  it;
        UINT *ipt;
        char  *cp;
    } realaddr;

    INTTYPE myval;
    char    reftype,
            obtype,
            myloc;

    realaddr.cp = &inpBuf[b3->r_offset];
    reftype = b3->r_flag;

    if (ob)     /* else L189a */
    {
        myval = ob->Offset;
        obtype = (char)(ob->Type);

        if ((reftype & RELATIVE))        /* else L18e0 */
        {
            myval -= bgn_codsiz;
        }
    }
    else
    {
        switch ((obtype = reftype) & 0x7)       /* L189a */
        {
            case 0:                 /*L18a5*/
                myval = bgn_udat;
                break;
            case INIENT:            /*L18a9*/
                myval = bgn_idat;
                break;
            case DIRENT:            /*L18ad*/
                myval = bgn_udpd;
                break;
            case DIRENT | INIENT:   /*L18b1*/
                myval = bgn_idpd;
                break;
            case CODENT:            /*L18b5*/
                myval = bgn_codsiz;
                break;
            default:                /*L18bb*/
                fatal("ref type error"
#ifndef COCO
                , 0, 0, 0
#endif
                );
        }
    }

    if (reftype & NEGMASK)
    {
        myval = -myval;
    }

    if ((myloc = (reftype & LOCMASK)) != CODLOC)
    {   /* It's a data def */
        realaddr.it += curObfil->codesiz;

        if (!(myloc & DIRLOC))  /* non-dpd */
        {
            realaddr.it += curObfil->IDpD;
        }
    }

    if (reftype & LOC1BYT)  /* else L1970 */
    {
        if (((obtype & 0x07) == CODENT) && (myloc == CODLOC))  /* else L1965 */
        {
            int vvtyp;

            if (((vvtyp = *realaddr.cp + myval) < -128) || (vvtyp > 127))
            {
                fatal("\npcr byte ref to '%s' out-of-range at $%04x in psect %s",
                        ob->sname, b3->r_offset, curObfil);
            }
        }

        *realaddr.cp += myval;
    }
    else
    {   /* L1970 */
#ifdef COCO
        *realaddr.ipt += myval;
#else
        *realaddr.ipt = o9_int(o9_int(*realaddr.ipt) + myval);
#endif
    }

    if (myloc != CODLOC)   /* else L19c4 */
    {
        register UINT dst;

        dst = (myloc & DIRLOC ? bgn_idpd : bgn_idat) + b3->r_offset;

        if ((obtype & 0x07) == CODENT)    /* else L19b0 */
        {
            getrefs(&dtxtrefs, dst);
            ++dtcount;
        }
        else
        {
            getrefs(&ddtarefs, dst);
            ++ddcount;
        }
    }
}

int
pty_gen(ip, c)
    register char *ip; int c;
{
    int c8_00 = -1;

    while (c--)
    {
        c8_00 ^= *(ip++);
    }

    return c8_00;
}

void
read_err(fn)
    char *fn;
{
    fatal("error reading file %s", fn
#ifndef COCO
            , 0, 0
#endif
            );
}

void
writ_err()
{
    if (ferror(OutFile))
    {
        fatal("error writing file %s", OutfName
#ifndef COCO
                , 0, 0
#endif
                );
    }
}

/* Temporary fix ??? */
#ifdef COCO
#asm
ftruncat pshs   x,u
      tfr    b,a
      ldb    #SS.Size
      ldx    #0
      ldu    6,s
      os9    I$SetStt
      puls   x,u
      lbra   _sysret
#endasm
#else

int
crc(src, count, dst)
    char *src;
    int count;
    char *dst;
{
	unsigned char   a;
    unsigned int idx;

    for (idx = 0; idx <count; idx++)
    {
        a = src[idx];
        a ^= dst[0];
        dst[0] = dst[1];
        dst[1] = dst[2];
        dst[1] ^= (a >> 7);
        dst[2] = (a << 1);
        dst[1] ^= (a >> 2);
        dst[2] ^= (a << 6);
        a ^= (a << 1);
        a ^= (a << 2);
        a ^= (a << 4);

        if (a & 0x80)
        {
            dst[0] ^= 0x80;
            dst[2] ^= 0x21;
        }
    }

	return 0;
}
#endif

#ifdef __LIL_END__
INTTYPE
getwrd(FILE *fp)
{
    INTTYPE wrd;

    if (fread(&wrd, 2, 1, fp) != 1)
        return EOF;
    else
        return ((wrd << 8) & 0xff00) | ((wrd >> 8) & 0xff);
}

int
putwrd(INTTYPE wrd, FILE *stream)
{
    if ((putc(wrd >> 8, stream) != EOF) && (putc(wrd & 0xff, stream) != EOF))
        return wrd;
    else
        return EOF;
}
#endif
