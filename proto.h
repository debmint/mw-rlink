/* rl_02.c */
void readrofs(void);
void L0937(binhead *bp, int do_vars);
void adj_ofst(void);
void adjsmofst(void);
void setbgns(void);
void goextref(OBFILES *ob);
void skiprefs(int nrefs);
int ncodlocs(register int maxcount);
void skipcomn(unsigned short vrsn);
SYMDAT *lastobfile(register SYMDAT *curob);
/* rl_03.c */
void wrtmodule(void);
void L1457(int parm1);
void L14d3(void);
void procref(def_ref *b3, SYMDAT *ob);
int pty_gen(register char *ip, int c);
void read_err(char *fn);
void writ_err(void);
int crc(char *src, int count, char *dst);
short getwrd(FILE *fp);
int putwrd(short wrd, FILE *stream);
/* rl_04.c */
int picklist(register char *str);
SYMDAT *matchsym(char *needle);
int L1bcf(SYMDAT *symbl);
int fatal(char *fmt, char *msg, char *msg2, char *msg3);
int req_mem(char *linpt);
void ref2base(register struct ext_ref *thisref);
struct ext_ref *findext(char *srchname, int findonly);
struct ext_ref *L1d61(char *nam);
void *get_mem(int req);
char *get_loc(int p1);
int writemap(void);
int unrsolved(void);
void getname(register char *dest, int maxsz);
char *_fgets(register char *dest, register FILE *fp);
int etrap(void);
/* rl_05.c */
void creatdnodes(int maxcount);
void setrefvoid(register unsigned short *mylist, int count);
void *emptyref(register short *reg01);
void notused(char *p1);
void getrefs(register struct dref_node **baseref, int addr);
void writrefs(register struct dref_node *lref);
void putref(int parm1);
void flushbuf(void);
/* rlink_main.c */
int main(int argc, char **argv);
char *nxtfnam(void);
char *nxtlib(void);
void illmainline(void);
void cantopen(void);
void notROF(void);
void asmerrs(void);
void toonew(void);
void twomains(void);
void in_rderr(void);
unsigned int o9_int(unsigned short val);
