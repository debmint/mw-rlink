#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#ifndef COCO
#   include <unistd.h>
#endif
#include "rlink.h"

direct struct ext_ref *ExtStart;

/*int    *D0164[64];
char *InFile;
char *OutfName;*/
extern struct ext_ref   curExtRef;
SYMDAT  *AllSyms[371];
extern direct int   printsym;
extern direct UINT t_code, t_idat, t_udat,  t_idpd,  t_udpd;

#ifdef COCO
struct ext_ref *findext();
void *get_mem();
#endif

int
picklist(str)
register char *str;
{
    UINT hashval = 0;

    while (*str)
    {
        hashval += *(str++);
    }

    return (hashval % 371);
}

/* Search for symbol with given name
 * Returns TRUE if found, FALSE if not */

SYMDAT *
matchsym(needle)
char *needle;
{
    register SYMDAT *haystack;

    haystack = AllSyms[picklist(needle)];

    while (haystack)
    {
        if (needle[0] == (haystack->sname)[0])
        {
            if (!strcmp(needle, haystack->sname))
                return haystack;
        }

        haystack = haystack->snext;
    }

    return 0;
}

int
L1bcf(symbl)
    SYMDAT *symbl;
{
    int idx;

    if (!matchsym(symbl->sname))        /* else L1c1f */
    {
        idx = picklist(symbl->sname);
        symbl->snext = AllSyms[idx];
        AllSyms[idx] = symbl;

        if (findext(symbl->sname, 0))
        {
            symbl->Type |= 0x100;
        }

        return 0;
    }

    return 1;
}

int
#ifdef __STDC__
fatal(char *fmt, char *msg, char *msg2, char *msg3)
#else
fatal (fmt, msg, msg2, msg3)
    char *fmt, *msg, *msg2, *msg3;
#endif
{
    int errcode = errno;

    fprintf (stderr, "linker fatal: ");
    fprintf (stderr, fmt, msg, msg2, msg3);
    fprintf (stderr, "\n");
    exit (errcode ? errcode : (errcode = 1));
}

int
#ifdef __STDC__
req_mem (char *linpt)
#else
req_mem (linpt)
    register char *linpt;
#endif
{
    UINT curchr;
    int ttl;

    ttl = 0;

    while (isdigit((curchr = *(linpt++))))
    {
        ttl = (ttl * 10) + (curchr - '0');
    }

    if ((curchr == 'k') || (curchr == 'K'))
    {
        ttl *= 4;
    }

    return ttl << 8;
}

/* Pull ref out of list and place in base of ExStart */

void
#ifdef __STDC__
ref2base (register struct ext_ref *thisref)
#else
ref2base (thisref)
    register struct ext_ref *thisref;
#endif
{
    thisref->prev->next = thisref->next;
    thisref->next->prev = thisref->prev;

    if (thisref != &curExtRef)
    {
        thisref->next = ExtStart;
        ExtStart = thisref;
    }
}

struct ext_ref *
#ifdef __STDC__
findext (char *srchname, int findonly)
#else
findext (srchname, findonly)
    char *srchname; int findonly;
#endif
{
    struct ext_ref *reftmp = 0;
    register struct ext_ref *listref = curExtRef.next;

    while (listref != &curExtRef)
    {
        if (listref->name[0] == srchname[0])   /* else L1d51 */
        {
            if ((strcmp (listref->name, srchname) == 0))  /* else L1d51 */
            {
                if (findonly)  /* else L1d46 */
                {
                    return listref;
                }
                else
                {
                    /* Pull this out of curExtRef and move to ExtStart */
                    reftmp = listref->next; /* Save place in search list */
                    ref2base (listref);
                    listref = reftmp;
                }
            }
        }

        listref = listref->next;
    }       /* L1d53 */

    return reftmp;
}

/* Find ref
 * If not in curExtRef, put it there */

struct ext_ref *
L1d61(nam)
    char *nam;
{
    register struct ext_ref *eref;

    /* If in curExtRef, return it */
    if ((eref = findext(nam, 1)))
        return eref;

    /* If not found, get ExtStart Item or create one if none */
    if (ExtStart)
    {
        eref = ExtStart;
        ExtStart = eref->next; /* Move next item to base */
    }
    else
    {
        eref = (struct ext_ref *)get_mem(sizeof(struct ext_ref));
    }

    /* Set up curExtRef pointers */
    eref->prev = &curExtRef;
    eref->next = curExtRef.next;
    return (curExtRef.next = curExtRef.next->prev = eref);
    /*curExtRef.next = eref;*/
    /*return eref;*/
}

void *
#ifdef __STDC__
get_mem (int req)
#else
get_mem(req)
    int req;
#endif
{
    void *rslt;
#ifdef COCO
    rslt = sbrk (req);
#else
    rslt = malloc (req);

    if (rslt == 0)
    {
        rslt = EOF;  /* For compatibility with COCO sbrk() */
    }
#endif
    if (rslt == EOF)
    {
        if (obmemreq)
        {
            fprintf (stderr, "need %d bytes for linkbuf\n" , obmemreq);
        }

        fatal ("out of memory"
#ifndef COCO
        , 0, 0, 0
#endif
        );
    }

    return rslt;    /* L1de2 */
}

char *
#ifdef __STDC__
get_loc(int p1)
#else
get_loc (p1)
    int p1;
#endif
{
    switch (p1 & 7)
    {
        case CODENT:     /* L1df9 */
            return "code";
        case 0:     /* L1dff */
            return "udat";
        case INIENT:            /* L1e05 */
            return "idat";
        case DIRENT:            /* L1e0b */
            return "udpd";
        case INIENT | DIRENT:   /* L1e11 */
            return "idpd";
        case CODENT | CONENT:   /* L1e17 */
            return "cnst";
        case CODENT | CONENT | SETENT:     /* L1e1d */
            return "comm";
        default:            /* L1e23 */
            return "???";
    }
}

direct char *D0005 = "     %-9s %s %04x\n";

int
writemap ()
{
    OBFILES *tt; /*Struct */

    printf ("Linkage map for %s  File - %s", modname, OutfName);
    puts ("\n\nSection          Code IDat UDat IDpD UDpD File\n");
    tt = FirstMod;

    while (tt)
    {
        if (tt->x19 & 0x100)
        {
            printf("%-16s %04x %04x %04x %02x   %02x %s\n",
                    tt->obname, tt->codesiz, tt->IDat, tt->UDat,
                    tt->IDpD, tt->UDpD, tt->roffile);

            if(printsym) /* else L1edb */
            {
                register SYMDAT *sd = tt->symlist;

                while (sd)
                {
                    printf(D0005, sd->sname, get_loc(sd->Type), sd->Offset);
                    sd = sd->sprev;
                }   /* L1ed7 */
            }
        }

        tt = tt->obn;
    }       /* L1ee0 */

    if (commnsyms)  /* else L1f13 */
    {
        register SYMDAT *rsd;

        printf("Common blocks:\n");
        rsd = commnsyms;

        while (rsd)
        {
            printf(D0005, rsd->sname, get_loc(rsd->Type), rsd->Offset);
            rsd = rsd->sprev;
        }
    }

    puts("                 ---- ---- ---- --");
    printf("                 %04x %04x %04x %02x  %02x\n",
            t_code, t_idat, t_udat,  t_idpd,  t_udpd);
}

int
unrsolved()
{
    register struct ext_ref *sd;

    if (curExtRef.next != &curExtRef)    /* else L1f9c */
    {
        fprintf(stderr, "Unresolved references:\n");
        sd = curExtRef.next;

        while (sd != &curExtRef)
        {
            fprintf(stderr, " %-16s %-16s in %-16.16s\n",       /* L219d */
                    sd->name, sd->myOb->obname, sd->myOb->roffile);
            sd = sd->next;
        }

        return 1;
    }

    return 0;
}

void
#ifdef __STDC__
getname (register char *dest, int maxsz)
#else
getname (dest, maxsz)
    register char *dest; int maxsz;
#endif
{
    int tmp;

    while (tmp = getc (InFile))
    {
        if (maxsz)
        {
            --maxsz;
            *dest++ = tmp;
        }
    }

    if (ferror (InFile))
    {
        in_rderr();
    }

    *dest = '\0';
}

char *
#ifdef __STDC__
_fgets (register char *dest, register FILE *fp)
#else
_fgets (dest, fp)
    register char *dest; FILE *fp;
#endif
{
    int tmp;

    while (((tmp = getc(fp)) != '\r') && (tmp != EOF))
    {
        *(dest++) = tmp;
    }

    if (tmp == EOF)
    {
        return 0;
    }

    *dest = '\0';
    return dest;
}

int
etrap()
{
    int errcode = errno;

    if (OutFile)
    {
        fclose(OutFile);
        unlink(OutfName);
    }

    exit(errno);
}
